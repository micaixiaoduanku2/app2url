package com.example.shareapp.ui;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpHelper {
	public String httpPost(String strUrl, String body) {
		URL url = null;

		String result = "";
		try {
			url = new URL(strUrl);
			HttpURLConnection urlConn = (HttpURLConnection) url
					.openConnection();

			urlConn.setDoInput(true);
			urlConn.setDoOutput(true);
			urlConn.setRequestMethod("POST");
			urlConn.setUseCaches(false);
			urlConn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			urlConn.setRequestProperty("Charset", "utf-8");
			DataOutputStream dop = new DataOutputStream(
					urlConn.getOutputStream());
			dop.write(body.getBytes("UTF-8"));
			dop.flush();
			dop.close();

			InputStreamReader in = new InputStreamReader(
					urlConn.getInputStream());
			BufferedReader bufferReader = new BufferedReader(in);
			String readLine = null;
			while ((readLine = bufferReader.readLine()) != null) {
				result += readLine;
			}
			in.close();
			urlConn.disconnect();
		} catch (Exception e) {
			result = "conn fail" + e.toString();
		} finally {
		}

		return result;
	}

	public String httpGet(String strUrl, String params) {
		URL url = null;

		String result = "";
		try {
			url = new URL(strUrl);
			HttpURLConnection urlConn = (HttpURLConnection) url
					.openConnection();
			InputStreamReader in = new InputStreamReader(
					urlConn.getInputStream());
			BufferedReader bufferReader = new BufferedReader(in);
			String readLine = null;
			while ((readLine = bufferReader.readLine()) != null) {
				result += readLine;
			}
			in.close();
			urlConn.disconnect();
		} catch (Exception e) {
			result = "conn fail" + e.toString();
		} finally {
		}

		return result;
	}
}
