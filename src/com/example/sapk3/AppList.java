package com.example.sapk3;

import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class AppList {

	AppList(PackageManager pm){
		this.init(pm);
	}
	
	private ArrayList<AppInfo> _appList = null;
	
	@SuppressLint("NewApi")
	private void init(PackageManager pm) {
		ArrayList<AppInfo> appList = new ArrayList<AppInfo>();
		List<PackageInfo> packages = pm.getInstalledPackages(0);
		
		for (int i = 0; i < packages.size(); i++) {
			PackageInfo packageInfo = packages.get(i);
            AppInfo appInfo = new AppInfo(pm, packageInfo);
            Intent li = pm.getLaunchIntentForPackage(appInfo.packageName);
            if(li != null){
                appList.add(appInfo);
            }
		}
		this._appList = appList;
	}
	
	public ArrayList<String> getPnames(){
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i < this._appList.size(); i++){
			list.add(this._appList.get(i).packageName);
		}
		return list;
	}
	
	public AppInfo get(int i){
		return this._appList.get(i);
	}
	
	public int size(){
		return this._appList.size();
	}
}
