package com.example.sapk3;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.shareapp.R;

public class MainActivity extends Activity {
	private Button button1;
	private TextView textView1;
    static String shorturl = "ori";
    static String sinashorturl = "ori";
    static String durl = "ori";
    static String sname = "";
	
    @SuppressLint("NewApi") @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sapk);
        
        button1 = (Button)findViewById(R.id.button1);
        textView1 = (TextView)findViewById(R.id.textView1);
        button1.setOnClickListener(new Button.OnClickListener(){ 
            @Override
            public void onClick(View v) {
            	Intent sendIntent = new Intent();  
            	sendIntent.setAction(Intent.ACTION_SEND);  
            	sendIntent.putExtra(Intent.EXTRA_TEXT, MainActivity.sname + " - " + MainActivity.sinashorturl );  
            	sendIntent.setType("text/plain");  
            	startActivity(sendIntent);  
            }         
        }); 

		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
//		if (savedInstanceState == null) {
//			getSupportFragmentManager().beginTransaction()
//					.add(R.id.container, new PlaceholderFragment()).commit();
//		}
        // Get intent, action and MIME type  
        Intent intent = getIntent();  
        String action = intent.getAction();  
        String type = intent.getType();  
  
        if (Intent.ACTION_SEND.equals(action) && type != null) {  
            if ("text/plain".equals(type)) {  
                handleSendText(intent); // Handle text being sent  
            } 
        } 
        
		AppList appList = new AppList(getPackageManager());
		ExistsChecker ec = new ExistsChecker(appList.getPnames());
		HashMap<String, Number> map = ec.check();
		Log.v("tjx", map.toString());
    }

    void handleSendText(Intent intent) {  
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);  
        String gpStr = "details?id=";
        String pname = sharedText.substring(sharedText.indexOf(gpStr) + gpStr.length());
        HttpHelper hh = new HttpHelper();
        String res = hh.httpGet("http://tjx.be/sapk/s.php?p=" + pname, "");
        JsonHelper jh = new JsonHelper();
        Map<String, String> map = jh.json_decode(res);
		MainActivity.sinashorturl = (String)map.get("sinashort");
		MainActivity.shorturl = (String)map.get("short");
		MainActivity.durl = (String)map.get("durl");
		MainActivity.sname = (String)map.get("sname");
		this.textView1.setText("turl: " + MainActivity.sinashorturl + "\ndurl: " + this.durl);
        if (sharedText != null) {  
            // Update UI to reflect text being shared  
        }  
    }  

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
